scalaVersion in ThisBuild := "2.13.1"
organization in ThisBuild := "ru.tinkoff.java2scala"

// https://docs.scala-lang.org/overviews/compiler-options/index.html
val scalacCompileOpts =  Seq(
  "-feature",
  "-unchecked",
  // "-deprecation:false", // uncomment if you *must* use deprecated apis
  "-Xfatal-warnings",
  "-Ywarn-value-discard",
)

lazy val assignment = project
  .settings(
    libraryDependencies ++= Seq(
      "com.typesafe" % "config" % "1.4.0",
      "com.iheart" %% "ficus" % "1.4.7",
      "org.slf4j" % "slf4j-api" % "1.7.25",
      "ch.qos.logback" % "logback-classic" % "1.2.3",
      "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2",
      "com.github.t3hnar" %% "scala-bcrypt" % "4.1",
      "org.scalatest" %% "scalatest" % "3.0.8" % Test
    ),
    scalacOptions in(Compile, compile) ++= scalacCompileOpts
  )

lazy val practice = project
  .settings(
    scalacOptions in(Compile, compile) ++= scalacCompileOpts
  )